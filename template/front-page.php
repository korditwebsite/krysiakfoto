<?php
/**
* Template name: Strona Główna
*/
?>
<?php get_header(); ?>
<main id="home">
	<section id="home-1">
		<div class="view">
			<?php if( have_rows('slider') ):
				while( have_rows('slider') ): the_row();
					?>
					<img src="<?php the_sub_field('obrazek'); ?>" class="img-fluid" alt="">
					<div class="mask ">
						<div class="slider-content">

							<div class="content">
						<!-- <h3><?php //the_sub_field('naglowek'); ?></h3>
							<p><?php //the_sub_field('opis'); ?></p> -->
							<script type="text/javascript">
								// if (screen && screen.width > 480) {
								// 	document.write('<a href="mailto:<?php //the_field('adres_e-mail', 'options'); ?>"><button class="btn btn-pink animated pulse infinite"><i style="margin-right: 10px;font-size: 1.25em;" class="fas fa-envelope"></i>Zamów sesję fotograficzną</button></a>')
								// }else
								// {
								// 	document.write('<a href="tel:+48<?php //the_field('numer_telefonu', 'options'); ?>"><button class="btn btn-pink animated pulse infinite"><i style="margin-right: 10px;font-size: 1.25em;" class="fas fa-phone-volume"></i>zamów sesję fotograficzną</button></a>')
								// }
							</script>
						</div>
					</div>
				</div>
			<?php endwhile; ?>
		<?php endif; ?>
	</div>
	<div class="gradient-trans"></div>
</section>
<section id="o-mnie">
	<div class="container">
		<div class="content">
			<div class="row d-flex align-items-center">
				<div class="col-lg-7">
					<?php if( have_rows('o_mnie') ):
						while( have_rows('o_mnie') ): the_row();
							?>
							<div class="text-area">
								<h3 class="title-section">O mnie</h3>
								<h4 class="desc-section"><?php the_sub_field('podpis_pod_tytulem'); ?></h4>
								<p><?php the_sub_field('tresc'); ?></p>
								<a style="color:#333;font-style: italic;" href="/o-mnie/">czytaj więcej</a>
								<h5 class="handwritten">Klementyna Krysiak</h5>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
				<div class="col-lg-5">
					<div class="thumbnail">
						<div class="human human-1"></div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="studio">
	<div class="container">
		<div class="col-lg-8 offset-lg-2 col-12">
			<div class="inner">
				<div class="content">
					<div>
						<h3 class="title-section">Studio</h3>
						<p><?php the_field('studio'); ?></p>
						<div class="flex-center">
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section id="oferta">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h3 class="title-section">Oferta</h3>
			</div>
		</div>
		<div class="row mt-4">
			<?php if(get_field('oferta')): ?>
				<?php while(has_sub_field('oferta')): ?>
					<div class="col-lg-6 mb-3">
						<div class="content rounded z-depth-1 ">
							<div class="lay-1">
								<img class="img-fluid " src="<?php the_sub_field('okladka'); ?>">
							</div>
							<div class="lay-2">
								<div>
									<a href="<?php the_sub_field('link'); ?>">
										<i class="fas fa-link"></i>
									</a>
									<h3><?php the_sub_field('nazwa'); ?></h3>
								</div>
							</div>
						</div>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</section>
<section id="cta">
	<div class="container">
		<div class="row">
			<?php if( have_rows('cta') ):
				while( have_rows('cta') ): the_row();
					?>
					<div class="col-md-12">
						<h3 class="title-section"><?php the_sub_field('tytul'); ?></h3>
						<p><?php the_sub_field('opis'); ?></p>
						<script type="text/javascript">
							if (screen && screen.width > 480) {
								document.write('<a href="mailto:<?php the_field('adres_e-mail', 'options'); ?>"><button class="btn btn-pink animated pulse infinite"><i style="margin-right: 10px;font-size: 1.25em;" class="fas fa-envelope"></i>Zamów sesję fotograficzną</button></a>')
							}else
							{
								document.write('<a href="tel:+48<?php the_field('numer_telefonu', 'options'); ?>"><button class="btn btn-pink animated pulse infinite"><i style="margin-right: 10px;font-size: 1.25em;" class="fas fa-phone-volume"></i>zamów sesję fotograficzną</button></a>')
							}
						</script>
					</div>
				<?php endwhile; ?>
			<?php endif; ?>
		</div>
	</div>
</section>
<section id="blog">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h3 class="title-section">Blog</h3>
			</div>
		</div>
		<div class="row">
				<?php global $post; // required
				$args = array('posts_per_page' => 3); // exclude category 9
				$custom_posts = get_posts($args);
				foreach($custom_posts as $post) : setup_postdata($post); ?>

					<div class="col-md-4 mb-5">
						<div class="post-loop">
							<div class="col-md-12">
								<div class="row">
									<div class="thumbnail-post">
										<img src="<?php echo get_the_post_thumbnail_url($post) ?>">
										<div class="post-date"><? echo date('d.m', strtotime($post->post_date)); ?> <? echo date('Y', strtotime($post->post_date)); ?></div>
									</div>
								</div>
								<div class="row justify-content-center">
									<h3><? echo $post->post_title; ?></h3>
								</div>
								<div class="row">

									<div class="post-date-line"></div>
								</div>
								<div class="row">
									<p class="max-word-height">
										<?echo wp_trim_words( $post->post_content,35,"…" ); ?>
									</p>
									<a class="justify-content-center" style="width: 100%;" href="<?php echo get_permalink($post,false) ?>"><button class="btn btn-outline-pink waves-effect">czytaj więcej</button></a>
								</div>
							</div>
						</div>
					</div>

				<?php endforeach; ?>
			</div>
		</div>
	</section>
	<section id="kontakt">
		<div class="container">
			<!--Grid row-->
			<div class="row">
				<!--Grid column-->
				<div class="col-md-12">
					<!--Form with header-->
					<div class="card">
						<div class="row d-flex align-items-center">
							<div class="col-lg-8">
								<div class="card-body form">
									<!--Header-->
									<div class="formHeader mb-1 pt-3">
										<h3>
											<i style="margin-right:10px;" class="fa fa-envelope"></i>Napisz do mnie</h3>
										</div>
										<?php echo do_shortcode( '[contact-form-7 id="4" title="Formularz 1"]' ); ?>
									</div>
								</div>
								<div class="col-lg-4">
									<div class="card-body contact text-center">
										<div class="container">
											<div class="mb-5">
												<h3>Dane kontaktowe</h3>
											</div>
											<ul class="contact-icons">
												<li class="adress">
													<i class="fa fa-map-marker"></i>
													<p><?php the_field('adres', 'options'); ?></p>
												</li>
												<li class="phone">
													<i class="fa fa-phone"></i>
													<a href="tel:+48<?php the_field('numer_telefonu', 'options'); ?>"><?php the_field('numer_telefonu', 'options'); ?></a>
												</li>
												<li class="mail">
													<i class="fa fa-envelope"></i>
													<a href="mailto:<?php the_field('adres_e-mail', 'options'); ?>"><?php the_field('adres_e-mail', 'options'); ?></a>
												</li>
											</ul>
											<hr class="hr-light mb-4 mt-4">
											<ul class="list-inline text-center list-unstyled">
												<li class="list-inline-item facebook">
													<a target="_blank" href="<?php the_field('link_fb', 'options'); ?>" class="p-2 m-2 fa-lg tw-ic">
														<i class="fab fa-facebook-f"></i>
													</a>
												</li>
												<li  class="list-inline-item instagram">
													<a href="<?php the_field('link_instagram', 'options'); ?>" target="_blank" class="p-2 m-2 fa-lg li-ic">
														<i class="fab fa-instagram"></i>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>
	</main>
	<?php get_footer(); ?>