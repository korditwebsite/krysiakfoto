<?php

/**
 * Include CSS files 
 */
function theme_enqueue_scripts() {
    
    wp_enqueue_style( 'Bootstrap_css', get_template_directory_uri() . '/css/bootstrap.min.css' );
    wp_enqueue_style( 'MDB', get_template_directory_uri() . '/css/mdb.min.css' );
    wp_enqueue_style( 'custom-css', get_template_directory_uri() . '/css/custom-css.css' );
    wp_enqueue_style( 'Style', get_template_directory_uri() . '/style.css' );
    wp_enqueue_style( 'mobile', get_template_directory_uri() . '/css/mobile.css' );
    wp_enqueue_script( 'jQuery', get_template_directory_uri() . '/js/jquery-3.1.1.min.js', array(), '2.2.3', true );
    wp_enqueue_script( 'Tether', get_template_directory_uri() . '/js/popper.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'Bootstrap', get_template_directory_uri() . '/js/bootstrap.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'MDB', get_template_directory_uri() . '/js/mdb.min.js', array(), '1.0.0', true );
    wp_enqueue_script( 'Custom-JS', get_template_directory_uri() . '/js/custom-js.js', array(), '1.0.0', true );
    wp_enqueue_script( 'Main', get_template_directory_uri() . '/js/main.js', array(), '1.0.0', true );

}
add_action( 'wp_enqueue_scripts', 'theme_enqueue_scripts' );


/**
 * Include external files
 */
require_once('inc/mdb_bootstrap_navwalker.php');
/**
 * Setup Theme
 */
function MDB_setup() {
  // Navigation Menus
  register_nav_menus(array(
    'navbar' => __( 'Navbar Menu')
));
  // Add featured image support
  add_theme_support('post-thumbnails');
    add_image_size('main-full', 1078, 516, false); // main post image in full width
}
add_action('after_setup_theme', 'MDB_setup');

/**
 * Register our sidebars and widgetized areas.
 */
function mdb_widgets_init() {

  register_sidebar( array(
    'name'          => 'sidebar',
    'id'            => 'sidebar',
    'before_widget' => '<div id="%1$s" class="widget-item">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="">',
    'after_title'   => '</h4>',
) );
  register_sidebar( array(
    'name'          => 'sidebar-blog',
    'id'            => 'sidebar',
    'before_widget' => '<div id="%1$s" class="widget-item">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="">',
    'after_title'   => '</h4>',
) );
  register_sidebar( array(
    'name'          => 'footer-1',
    'id'            => 'footer-1',
    'before_widget' => '<div id="%1$s" class="widget-item">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="">',
    'after_title'   => '</h4>',
) );
  register_sidebar( array(
    'name'          => 'footer-2',
    'id'            => 'footer-2',
    'before_widget' => '<div id="%1$s" class="widget-item">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="">',
    'after_title'   => '</h4>',
) );
  register_sidebar( array(
    'name'          => 'footer-3',
    'id'            => 'footer-3',
    'before_widget' => '<div id="%1$s" class="widget-item">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="">',
    'after_title'   => '</h4>',
) );
  register_sidebar( array(
    'name'          => 'footer 4',
    'id'            => 'footer-4',
    'before_widget' => '<div id="%1$s" class="widget-item">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="">',
    'after_title'   => '</h4>',
) );
  register_sidebar( array(
    'name'          => 'sidebar-oferta',
    'id'            => 'sidebar-oferta',
    'before_widget' => '<div id="%1$s" class="widget-item">',
    'after_widget'  => '</div>',
    'before_title'  => '<h4 class="">',
    'after_title'   => '</h4>',
) );
}
add_action( 'widgets_init', 'mdb_widgets_init' );

function theme_prefix_setup() {
  
  add_theme_support( 'custom-logo', array(
    'height'      => 120,
    'width'       => 400,
    'flex-width' => true,
) );

}
add_action( 'after_setup_theme', 'theme_prefix_setup' );

function short_filter_wp_title( $title ) {
    if ( is_single() || ( is_home() && !is_front_page() ) || ( is_page() && !is_front_page() ) ) {
        $title = single_post_title( '', false );
    }
    if ( is_front_page() && ! is_page() ) {
        $title = esc_attr( get_bloginfo( 'name' ) );
    }
    return $title;
    add_filter( 'wp_title', 'short_filter_wp_title', 100);
    
}
if ( function_exists( 'add_theme_support' ) ) {
    add_theme_support( 'post-thumbnails' );
}
?>

<?php 
add_action('init', 'studio_register');
function studio_register() {
   
  $labels = array(
    'name' => _x('Fotografia studyjna', 'post type general name'),
    'singular_name' => __( 'Fotografia studyjna' ),
    'add_new' => _x('Add New', 'Dodaj ofertę'),
    'add_new_item' => __('Dodaj ofertę'),
    'edit_item' => __('Edytuj ofertę'),
    'new_item' => __('Dodaj ofertę'),
    'view_item' => __('Zobacz ofertę'),
    'search_items' => __('Znajdź ofertę'),
    'not_found' =>  __('Nie znaleziono oferty'),
    'not_found_in_trash' => __('Nic nie znaleziono w koszu'),
    'add_new' => __( 'Dodaj ofertę' ),
    
);
  
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array('title','editor','thumbnail'),
    'post_id' => 'studio',
    'position' => "2",
    'menu_icon'   => 'dashicons-camera',
    'post_type' => 'studio'

); 
  register_post_type( 'fotografia-studyjna' , $args );
}
?>


<?php 
add_action('init', 'plener_register');
function plener_register() {
   
  $labels = array(
    'name' => _x('Sesja plenerowa', 'post type general name'),
    'singular_name' => __( 'Sesja plenerowa' ),
    'add_new' => _x('Add New', 'Dodaj ofertę'),
    'add_new_item' => __('Dodaj ofertę'),
    'edit_item' => __('Edytuj ofertę'),
    'new_item' => __('Dodaj ofertę'),
    'view_item' => __('Zobacz ofertę'),
    'search_items' => __('Znajdź ofertę'),
    'not_found' =>  __('Nie znaleziono oferty'),
    'not_found_in_trash' => __('Nic nie znaleziono w koszu'),
    'add_new' => __( 'Dodaj ofertę' ),
    
);
  
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array('title','editor','thumbnail'),
    'post_id' => 'plener',
    'position' => "2",
    'menu_icon'   => 'dashicons-camera',
    'post_type' => 'plener'

); 
  register_post_type( 'sesje-plenerowe' , $args );
}
?>

<?php 
add_action('init', 'fotoreportaz_register');
function fotoreportaz_register() {
   
  $labels = array(
    'name' => _x('Fotoreportaż', 'post type general name'),
    'singular_name' => __( 'Fotoreportaż' ),
    'add_new' => _x('Add New', 'Dodaj ofertę'),
    'add_new_item' => __('Dodaj ofertę'),
    'edit_item' => __('Edytuj ofertę'),
    'new_item' => __('Dodaj ofertę'),
    'view_item' => __('Zobacz ofertę'),
    'search_items' => __('Znajdź ofertę'),
    'not_found' =>  __('Nie znaleziono oferty'),
    'not_found_in_trash' => __('Nic nie znaleziono w koszu'),
    'add_new' => __( 'Dodaj ofertę' ),
    
);
  
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array('title','editor','thumbnail'),
    'post_id' => 'fotoreportaz',
    'position' => "2",
    'menu_icon'   => 'dashicons-camera',
    'post_type' => 'fotoreportaz'

); 
  register_post_type( 'fotoreportaz' , $args );
}
?>
<?php 
add_action('init', 'grafika_register');
function grafika_register() {
   
  $labels = array(
    'name' => _x('Usługi graficzne', 'post type general name'),
    'singular_name' => __( 'Usługi graficzne' ),
    'add_new' => _x('Add New', 'Dodaj ofertę'),
    'add_new_item' => __('Dodaj ofertę'),
    'edit_item' => __('Edytuj ofertę'),
    'new_item' => __('Dodaj ofertę'),
    'view_item' => __('Zobacz ofertę'),
    'search_items' => __('Znajdź ofertę'),
    'not_found' =>  __('Nie znaleziono oferty'),
    'not_found_in_trash' => __('Nic nie znaleziono w koszu'),
    'add_new' => __( 'Dodaj ofertę' ),
    
);
  
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array('title','editor','thumbnail'),
    'post_id' => 'grafika',
    'position' => "2",
    'menu_icon'   => 'dashicons-admin-customizer',
    'post_type' => 'grafika'

); 
  register_post_type( 'uslugi-graficzne' , $args );
}
?>
<?php 
add_theme_support( 'yoast-seo-breadcrumbs' );
?>

<?php 
if( function_exists('acf_add_options_page') ) {
    
    acf_add_options_page(array(
        'page_title'    => 'Opcje motywu',
        'menu_title'    => 'Opcje motywu',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'      => false
    ));
}
?>

<?php 
add_action('init', 'klient_register');
function klient_register() {
   
  $labels = array(
    'name' => _x('Strefa klienta', 'post type general name'),
    'singular_name' => __( 'Strefa klienta' ),
    'add_new' => _x('Add New', 'Dodaj klienta'),
    'add_new_item' => __('Dodaj klienta'),
    'edit_item' => __('Edytuj klienta'),
    'new_item' => __('Dodaj klienta'),
    'view_item' => __('Zobacz klienta'),
    'search_items' => __('Znajdź klienta'),
    'not_found' =>  __('Nie znaleziono oferty'),
    'not_found_in_trash' => __('Nic nie znaleziono w koszu'),
    'add_new' => __( 'Dodaj klienta' ),
    
);
  
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array('title','editor','thumbnail'),
    'post_id' => 'grafika',
    'position' => "2",
    'menu_icon'   => 'dashicons-groups',
    'post_type' => 'klient'

); 
  register_post_type( 'strefa-klienta' , $args );
}
?>