<?php
/**
* Template name: Kontakt
*/
?>
<?php get_header(); ?>
<main class="kontakt">
	<section>
		<div class="container">
			<div class="content">
				<div class="row">
					<div class="col-md-12 mt-4">
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<section id="kontakt">
							<div class="container">
								<!--Grid row-->
								<div class="row">
									<!--Grid column-->
									<div class="col-md-12">
										<!--Form with header-->
										<div class="card">
											<div class="row d-flex align-items-center">
												<div class="col-lg-8">
													<div class="card-body form">
														<!--Header-->
														<div class="formHeader mb-1 pt-3">
															<h3>
															<i style="margin-right:10px;" class="fa fa-envelope"></i>Napisz do mnie</h3>
														</div>
														<?php echo do_shortcode( '[contact-form-7 id="4" title="Formularz 1"]' ); ?>
													</div>
												</div>
												<div class="col-lg-4">
													<div class="card-body contact text-center">
														<div class="container">
															<div class="mb-5">
																<h3>Dane kontaktowe</h3>
															</div>
															<ul class="contact-icons">
																<li class="adress">
																	<i class="fa fa-map-marker"></i>
																	<p><?php the_field('adres', 'options'); ?></p>
																</li>
																<li class="phone">
																	<i class="fa fa-phone"></i>
																	<a href="tel:+48<?php the_field('numer_telefonu', 'options'); ?>"><?php the_field('numer_telefonu', 'options'); ?></a>
																</li>
																<li class="mail">
																	<i class="fa fa-envelope"></i>
																	<a href="mailto:<?php the_field('adres_e-mail', 'options'); ?>"><?php the_field('adres_e-mail', 'options'); ?></a>
																</li>
															</ul>
															<hr class="hr-light mb-4 mt-4">
															<ul class="list-inline text-center list-unstyled">
																<li class="list-inline-item facebook">
																	<a target="_blank" href="<?php the_field('link_fb', 'options'); ?>" class="p-2 m-2 fa-lg tw-ic">
																		<i class="fab fa-facebook-f"></i>
																	</a>
																</li>
																<li  class="list-inline-item instagram">
																	<a href="<?php the_field('link_instagram', 'options'); ?>" target="_blank" class="p-2 m-2 fa-lg li-ic">
																		<i class="fab fa-instagram"></i>
																	</a>
																</li>
															</ul>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</section>
					</div>
				</div>
			</div>
		</div>
	</section>
	<section id="cta">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<h1 class="font-weight-bold mb-3">Parnterzy</h1>
				</div>
			</div>
			<div class="row">
				<?php if(get_field('partnerzy')): ?>
				<?php while(has_sub_field('partnerzy')): ?>
				<div class="col-lg-3">
					<a target="_blank" href="<?php the_sub_field('link'); ?>">
					<img class="img-fluid mb-3" src="<?php the_sub_field('img'); ?>">
					</a>
				</div>
				<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
	<section id="map">
		<div class="container-fluid">
			<div class="row">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2515.5623089774303!2d18.943959815995104!3d50.91331437954188!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4710bbea893d0ce7%3A0xa2a3813fa90e7804!2zR2VuZXJhxYJhIFfFgmFkeXPFgmF3YSBBbmRlcnNhIDE2LCA0Mi0xMDAgS8WCb2J1Y2s!5e0!3m2!1spl!2spl!4v1524588897417" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>