<?php
/**
* Template name: Blog
*/
?>
<?php get_header(); ?>
<main id="blog-page">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<section id="blog">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<h3 class="title-section mt-5 mb-3">Blog</h3>
							</div>
						</div>
						<div class="row">
							<?php global $post; // required
							$args = array('posts_per_page' => 9); // exclude category 9
							$custom_posts = get_posts($args);
							foreach($custom_posts as $post) : setup_postdata($post); ?>
							
							<div class="col-md-12 mb-5 card">
								<div class="post-loop">
									<div class="col-md-12">
										<div class="row">
											<div class="thumbnail-post">
												<img src="<?php echo get_the_post_thumbnail_url($post) ?>">
												<div class="post-date"><? echo date('d.m', strtotime($post->post_date)); ?> <? echo date('Y', strtotime($post->post_date)); ?></div>
											</div>
										</div>
										<div class="row justify-content-center">
											<h3><? echo $post->post_title; ?></h3>
										</div>
										<div class="row">
											
											<div class="post-date-line"></div>
										</div>
										<div class="row">
											<p class="max-word-height">
												<?echo wp_trim_words( $post->post_content,35,"…" ); ?>
											</p>
											<a class="justify-content-center" style="width: 100%;" href="<?php echo get_permalink($post,false) ?>"><button class="btn btn-outline-pink waves-effect">czytaj więcej</button></a>
										</div>
									</div>
								</div>
							</div>
							
							<?php endforeach; ?>
						</div>
					</div>
				</section>
			</div>
			<div class="col-md-3">
				<div class="sidebar card">
					<?php dynamic_sidebar('sidebar-blog'); ?>
				</div>
			</div>
		</div>
	</div>
	<section id="cta">
		<div class="container">
			<div class="row">
				<?php if( have_rows('cta', 'options') ):
					while( have_rows('cta', 'options') ): the_row();
				?>
				<div class="col-md-12 d-flex justify-content-center">
					<div>
						<h3 class="title-section"><?php the_sub_field('tytul'); ?></h3>
						<p><?php the_sub_field('opis'); ?></p>
						<a href="tel:+48<?php the_field('numer_telefonu', 'options'); ?>">
						<button class="btn btn-pink animated pulse infinite"><i style="margin-right: 10px;font-size: 1.25em;" class="fas fa-phone-volume"></i>zamów sesję fotograficzną</button></a>
					</div>
				</div>
				<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>