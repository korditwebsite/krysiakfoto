<?php
/**
* Template name: Studio
*/
?>
<?php get_header(); ?>
<main id="studio-page">
	<?php if( have_rows('o_mnie') ):
									while( have_rows('o_mnie') ): the_row();
	?>
	<section>
		<div class="container">
			<div class="content">
				<div class="row">
					<div class="col-md-12 mt-4">
					</div>
				</div>
				<div class="row card mb-4">
					<div class="col-md-12">
						<div class="content">
							<div class="row flex-v-bottom">
								<div class="col-lg-7">
									
									<div class="text-area">
										<h3 class="title-section">O mnie</h3>
										<h4 class="desc-section"><?php the_sub_field('podpis_pod_tytulem'); ?></h4>
										<p><?php the_sub_field('tresc'); ?></p>
										<h5 class="handwritten">Klementyna Krysiak</h5>
									</div>
									
								</div>
								<div class="col-lg-5">
									<div class="thumbnail">
										<div class="human human-1"></div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="studio-bg">
			<div class="container">
				<div class="col-md-12 card">
					<div class="row flex-center">
						<div class="col-md-6">
							<img class="img-fluid rounded z-depth-2" src="<?php the_sub_field('zdjecie_studio'); ?>">
						</div>
						<div class="col-md-6">
							<h3 class="title-section">O Studio</h3>
							<p><?php the_sub_field('opis-studio'); ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php endwhile; ?>
	<?php endif; ?>
	<section id="cta">
		<div class="container">
			<div class="row">
				<?php if( have_rows('cta', 'options') ):
					while( have_rows('cta', 'options') ): the_row();
				?>
				<div class="col-md-12 d-flex justify-content-center">
					<div>
						<h3 class="title-section"><?php the_sub_field('tytul'); ?></h3>
						<p><?php the_sub_field('opis'); ?></p>
						<script type="text/javascript">
							if (screen && screen.width > 480) {
						document.write('<a href="mailto:<?php the_field('adres_e-mail', 'options'); ?>"><button class="btn btn-pink animated pulse infinite"><i style="margin-right: 10px;font-size: 1.25em;" class="fas fa-envelope"></i>Zamów sesję fotograficzną</button></a>')
						}else
						{
						document.write('<a href="tel:+48<?php the_field('numer_telefonu', 'options'); ?>"><button class="btn btn-pink animated pulse infinite"><i style="margin-right: 10px;font-size: 1.25em;" class="fas fa-phone-volume"></i>zamów sesję fotograficzną</button></a>')
						}
						</script>
					</div>
				</div>
				<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>