<?php
/**
* Template name: Domyślna podstrona
*/
?>
<?php get_header(); ?>
<main class="page">
	<section id="hero-page" class="post-loop mb-5">
	</section>
	<section class="page-template mb-5">
		<div class="container">
			<div class="row">
				<div class="col-md-9">
					<div class="content-service">
						<?php if( has_post_thumbnail() ):?>
						<figure class="featured-image" title="<?php echo esc_attr( sprintf( __( 'Permalink to %s', 'catch-everest' ), the_title_attribute( 'echo=0' ) ) ); ?>">
								<?php the_post_thumbnail( 'featured' ); ?>
						</figure>
						<?php endif; ?>
						<h1><?php echo short_filter_wp_title( $title ); ?></h1>
						<?php echo the_content(); ?>
					</div>
				</div>
				<div class="col-md-3">
					<?php echo get_sidebar(); ?>
				</div>
			</div>
		</div>
	</section>
	<section>
		<div class="container">
			<h2><?php the_field('tytul_realizacji'); ?></h2>
		</div>
	</section>
	<section id="cta">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p><strong>Zainteresowany?</strong> Skontaktuj się przez formularz kontaktowy i zamów wybraną usługę!</p>
				</div>
				<div class="col-md-12">
					<a class="corner-button" href="/zamow"><button><span>Przejdź do zamówienia</span></button></a>
				</div>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>