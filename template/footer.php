<!--Footer-->
<footer class="page-footer black accent-3 center-on-small-only">
    <!--Footer Links-->
    <div class="container">
        <div class="row">
            <!--Second column-->
            <div class="col-lg-3 col-md-2 col-12">
                <div class="item-footer-1">
                    <?php dynamic_sidebar('footer-1'); ?>
                </div>
            </div>
            <!--/.Second column-->
            <!--Third column-->
            <div class="col-lg-3 col-md-4 col-12">
                <div class="item-footer item-footer-2">
                    <?php dynamic_sidebar('footer-2'); ?>
                </div>
            </div>
            <!--/.Third column-->
            <!--Fourth column-->
            <div class="col-lg-3 col-md-3 col-12">
                <div class="item-footer item-footer-3">
                    <?php dynamic_sidebar('footer-3'); ?>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-12">
                <div class="item-footer item-footer-4 ">
                    <?php dynamic_sidebar('footer-4'); ?>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="footer-copyright">
                <div class="container mt-3">
                    <h5 style="color:#fff;">Wszelkie prawa zastrzeżone© 2018
                        <a target="_blank" href="https://icomweb.pl"><img style="max-width: 110px;" src="http://ngbklobuck.pl/wp-content/uploads/2018/07/logo-1.png"></a>
                    </h5>
                    
                </div>
            </div>
        </div>
    </div>
</footer>
<!--/.Footer-->
<?php wp_footer(); ?>
<script defer src="https://use.fontawesome.com/releases/v5.0.8/js/all.js" integrity="sha384-SlE991lGASHoBfWbelyBPLsUlwY1GwNDJo3jSJO04KZ33K2bwfV9YBauFfnzvynJ" crossorigin="anonymous"></script>
</body>
</html>