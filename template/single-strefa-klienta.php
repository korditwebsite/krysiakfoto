<?php
/**
Template Name: Strefa klienta
Template Post Type: post, page, product
*/
?>
<?php get_header(); ?>
<style type="text/css">
	ul.thumbnails.image_picker_selector {
overflow: auto;
list-style-image: none;
list-style-position: outside;
list-style-type: none;
padding: 0px;
margin: 0px; }
ul.thumbnails.image_picker_selector ul {
overflow: auto;
list-style-image: none;
list-style-position: outside;
list-style-type: none;
padding: 0px;
margin: 0px; }
ul.thumbnails.image_picker_selector li.group {width:100%;}
ul.thumbnails.image_picker_selector li.group_title {
float: none; }
ul.thumbnails.image_picker_selector li {
margin: 0px 12px 12px 0px;
float: left; }
ul.thumbnails.image_picker_selector li .thumbnail {
padding: 6px;
border: 1px solid #dddddd;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none; }
ul.thumbnails.image_picker_selector li .thumbnail img {
-webkit-user-drag: none; }
ul.thumbnails.image_picker_selector li .thumbnail.selected {
background: #0088cc; }
input[type="submit"] {
   background: #fc2d8f;
    border: none;
    color: #fff;
    text-transform: uppercase;
    padding: 10px 20px;
    display: block;
    margin-top: 20px;
}
</style>
<main class=" mt-5 mb-3" id="service-item-page">
	<div class="container">
		<div class="row">
			<?php if( !post_password_required( $post )): ?>
			<div class="col-md-12">
				<div class="content-service card" id="blog">
					<div class="thumbnail-service">
					</div>
					<h5 class="title-section">Zdjęcia dla <strong><?php echo short_filter_wp_title( $title ); ?></strong></h5>
					<?php
					$liczba = get_field('limit');
					if ($liczba == 1) {
					$odmiana = 'zdjęcie';
					}
					else if ($liczba == 2  || $liczba == 3) {
					$odmiana = 'zdjęcia';
					}
					else if ($liczba >=  4) {
					$odmiana = 'zdjęć';
					}
					$imie = short_filter_wp_title( $title );
					?>

					<p>Pamiętaj, możesz wybrać <strong><?php the_field('limit'); ?></strong> <?php echo $odmiana ?>. Wybierz je, klikając na nie</p> 
					<div class="zdjecia">
						
						<div class="col-md-12">
							<form action="/klient.php" method="POST">
								<div class="row">
									<select multiple="multiple" name="wybierz[]" data-limit="<?php the_field('limit'); ?>" class="image-picker">
										<?php if(get_field('repeat')):
										$i = 1;
										?>
										<?php while(has_sub_field('repeat')): ?>
										<option data-img-src="<?php the_sub_field('img'); ?>" value="<?php the_sub_field('img'); ?>"></option>
										<?php
										$i = $i+1;
										endwhile; ?>
										<?php endif; ?>
									</div>
								</select>
								<input type="hidden" name="imie" value="<?php echo $imie; ?>">
								<input type='submit' name='submit' value="prześlij swój wybór" />
							</form>
						</div>
					</div>
				</div>
			</div>
			<?php endif; ?>
<?php the_content(); ?>
		</div>
	</div>
</main>
<?php get_footer(); ?>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/image-picker/0.2.4/image-picker.min.css">
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/image-picker/0.2.4/image-picker.min.js"></script>
<script type="text/javascript">
	$("select").imagepicker({
hide_select : true,
show_label  : false
})
</script>