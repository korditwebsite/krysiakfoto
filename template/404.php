<?php get_header(); ?>
<main class="no-found">
	<section id="hero-page" class="post-loop mb-5">
	</section>
	<section class="page-template">
		<div class="container">
			<div class="row">
				<div class="center-flex">
					<div class="col-md-12">
						<div class="row">
							<h1 class="text-center">Strona nie została znaleziona</h1>
						</div>
						<div class="row">
							<a class="corner-button corner-button-dark" href="/"><button><span></span>Wróć do strony głównej</button></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>