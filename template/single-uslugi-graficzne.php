<?php
/**
Template Name: Usługi graficzne
Template Post Type: post, page, product
*/
?>
<?php get_header(); ?>

<main class=" mt-5 mb-3" id="service-item-page">
	<div class="container">
		<div class="row">
			<div class="col-md-9">
				<div class="content-service card" id="blog">
					<div class="thumbnail-service">
						<?php
													if ( has_post_thumbnail()  ) {
													the_post_thumbnail( 'home-thumbnail' );
													}
						?>
					</div>
					<h3 class="title-section"><?php echo short_filter_wp_title( $title ); ?></h3>
					<div class="text-service">
						<?php the_content(); ?>
					</div>
					<div class="portfolio  mt-5">
						<h3 class="title-section">Sprawdź moje realizacje</h3>
						<div class="realizacje">
							<?php the_field('realizacje'); ?>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="sidebar card">
					<h4 class="title-sidebar">Sprawdź moje pozostałe usługi</h4>
					<?php dynamic_sidebar('sidebar-oferta'); ?>
				</div>
			</div>
		</div>
	</div>
	<section id="cta">
		<div class="container">
			<div class="row">
				<?php if( have_rows('cta', 'options') ):
					while( have_rows('cta', 'options') ): the_row();
				?>
				<div class="col-md-12 d-flex justify-content-center">
					<div>
						<h3 class="title-section"><?php the_sub_field('tytul'); ?></h3>
						<p><?php the_sub_field('opis'); ?></p>
						<script type="text/javascript">
							if (screen && screen.width > 480) {
						document.write('<a href="mailto:<?php the_field('adres_e-mail', 'options'); ?>"><button class="btn btn-pink animated pulse infinite"><i style="margin-right: 10px;font-size: 1.25em;" class="fas fa-envelope"></i>Zamów sesję fotograficzną</button></a>')
						}else
						{
						document.write('<a href="tel:+48<?php the_field('numer_telefonu', 'options'); ?>"><button class="btn btn-pink animated pulse infinite"><i style="margin-right: 10px;font-size: 1.25em;" class="fas fa-phone-volume"></i>zamów sesję fotograficzną</button></a>')
						}
						</script>
					</div>
				</div>
				<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>