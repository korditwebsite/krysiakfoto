<!DOCTYPE html>
<html <?php language_attributes(); ?>>
  <head>
    <!-- Required meta tags always come first -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/modernizr/2.8.3/modernizr.min.js" type="text/javascript"></script>
    <title><?php wp_title('the_title_attribute();'); ?></title>
    <?php wp_head(); ?>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
    <!-- Facebook Pixel Code -->
    <!-- End Facebook Pixel Code -->
  </head>
  <body <?php body_class( 'class-name' ); ?>>
    <!-- Main navigation -->
    <header>
      <!-- Navbar -->
      <nav class="navbar navbar-expand-lg my-navbar">
        <div class="container">
          <div class="col-lg-12">
            <div class="bottom-line">
              <a class="navbar-brand" href="/">
                <?php $custom_logo_id = get_theme_mod( 'custom_logo' );
                $logo = wp_get_attachment_image_src( $custom_logo_id , 'full' );
                if ( has_custom_logo() ) {
                echo '<img src="'. esc_url( $logo[0] ) .'">';
                } else {
                echo '<h1>'. get_bloginfo( 'name' ) .'</h1>';
                } ?>
              </a>
            </div>
            <div class="button-open">
              <div id="open-window">
                <i class="fas fa-bars"></i>
              </div>
            </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent-7">
              <?php
              wp_nav_menu( array(
              'menu'            => 'navbar',
              'menu_class'      => 'navbar-nav smooth-scroll mdb-navigation',
              'depth' => 2,
              // 'link_before'     => '<b>',
              // 'link_after'      => '</b>',
              'walker'   => new BootstrapNavMenuWalker()
              ));
              ?>
            </div>
          </div>
          <div class="mobile-menu">
            <div id="close-window">
              <i class="far fa-times-circle"></i>
            </div>
            <?php
            wp_nav_menu( array(
            'menu'            => 'navbar',
            'menu_class'      => 'navbar-nav smooth-scroll mdb-navigation',
            'depth' => 2,
            // 'link_before'     => '<b>',
            // 'link_after'      => '</b>',
            'walker'   => new BootstrapNavMenuWalker()
            ));
            ?>
          </div>
        </div>
      </div>
    </nav>
  </header>
  <!-- Navbar -->
  <!-- Full Page Intro -->
  <?php the_field('header_title', 'option'); ?>