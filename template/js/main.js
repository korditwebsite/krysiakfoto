$("ul.mdb-navigation > li").addClass("nav-item")
$("ul.mdb-navigation > li > a").addClass("nav-link")

$( ".wow" ).addClass( "fadeIn" );

wow = new WOW({
    boxClass: 'wow', // default
    animateClass: 'animated', // default
    offset: 0, // default
    mobile: true, // default
    live: true // default
})
wow.init();

$( "#open-window" ).click(function() {
	$( ".mobile-menu" ).toggleClass( "show-menu" );
});
$( "#close-window" ).click(function() {
	$( ".mobile-menu" ).removeClass( "show-menu" );
});


jQuery('.skillbar').each(function(){
	jQuery(this).find('.skillbar-bar').animate({
		width:jQuery(this).attr('data-percent')
	},2000);
});

$('body').on('mouseenter mouseleave','.dropdown',function(e){
	var _d=$(e.target).closest('.dropdown');_d.addClass('show');
	setTimeout(function(){
		_d[_d.is(':hover')?'addClass':'removeClass']('show');
		$('[data-toggle="dropdown"]', _d).attr('aria-expanded',_d.is(':hover'));
	},300);
});

$(function(){
	$('.human').hover(function() {
		$('.human').addClass('human-2');
		$('.human').removeClass('human-1');
	}, function() {
		$('.human').addClass('human-1');
		$('.human').removeClass('human-2');
	})
})

$("#oferta .content").mouseenter(
	function(){ $(this).addClass('show') },
	);
$("#oferta .content").mouseleave(
	function(){ $(this).removeClass('show') },
	);


$(function() {
	$(this).bind("contextmenu", function(e) {
		e.preventDefault();
	});
}); 