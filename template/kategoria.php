<?php
/**
* Template name: Kategoria
*/
?>
<?php get_header(); ?>
<main id="category-page">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<section id="blog">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<h3 class="title-section mt-5 mb-3"><?php echo short_filter_wp_title( $title ); ?></h3>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<section id="oferta">
									<?php
									$nazwaKategori = get_field('kategoria');
									$args = array(
									'post_type'   => $nazwaKategori,
									'post_status' => 'publish',
									'order' => 'ASC'
									);
									
									$testimonials = new WP_Query( $args );
									if( $testimonials->have_posts() ) :
									?>
									<div class="row mt-4">
										<?php
															while( $testimonials->have_posts() ) :
															$testimonials->the_post();
										?>
										<div class="col-lg-6 mb-3">
											<div class="content rounded z-depth-1 ">
												<div class="lay-1">
													<?php
													if ( has_post_thumbnail()  ) {
													the_post_thumbnail( 'home-thumbnail' );
													}
													?>
												</div>
												<div class="lay-2">
													<div>
														<a href="<?php the_permalink(); ?>">
															<i class="fas fa-link"></i>
														</a>
														<h3><?php printf(get_the_title());  ?></h3>
													</div>
												</div>
											</div>
										</div>
										<?php
										endwhile;
										wp_reset_postdata();
										?>
										<?php
										else :
										esc_html_e( 'Ta kategoria jest pusta, uzupełnij ją!', 'text-domain' );
										endif;
										?>
									</div>
								</section>
							</div>
						</div>
					</div>
				</section>
			</div>
			<!-- <div class="col-md-3">
				<div class="sidebar">
					<?php //dynamic_sidebar('sidebar-oferta'); ?>
				</div>
			</div> -->
		</div>
	</div>
	<section id="cta">
		<div class="container">
			<div class="row">
				<?php if( have_rows('cta', 'options') ):
					while( have_rows('cta', 'options') ): the_row();
				?>
				<div class="col-md-12 d-flex justify-content-center">
					<div>
						<h3 class="title-section"><?php the_sub_field('tytul'); ?></h3>
						<p><?php the_sub_field('opis'); ?></p>
						<a href="tel:+48<?php the_field('numer_telefonu', 'options'); ?>">
						<button class="btn btn-pink animated pulse infinite"><i style="margin-right: 10px;font-size: 1.25em;" class="fas fa-phone-volume"></i>zamów sesję fotograficzną</button></a>
					</div>
				</div>
				<?php endwhile; ?>
				<?php endif; ?>
			</div>
		</div>
	</section>
</main>
<?php get_footer(); ?>